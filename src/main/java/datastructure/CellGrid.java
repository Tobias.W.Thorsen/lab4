package datastructure;

import java.lang.reflect.Array;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows; // høyde
    private int cols; // bredde
    private CellState[][] grid; // antall ruter (celler) med CellState som verdier.
    // eller ArrayList<Optional<T>> grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// Auto-generated constructor stub
        this.cols = columns;
        this.rows = rows;
        grid = new CellState[rows][cols];
        for (int i=0; i<rows; i++){
            for (int j=0; j<cols; j++) {
                grid[i][j] = initialState;
            }
        }
	}
    /*private int indexOf(int row, int col){
        return col+row*cols;
    }*/

    @Override
    public int numRows() {
        // Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i<rows; i++) {
            for (int j = 0; j<cols; j++) {
                copy.set(i, j, get(i, j));
            }
        }
        return copy;
    }
    
}
